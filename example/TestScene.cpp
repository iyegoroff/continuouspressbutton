/*
 * Copyright (C) 2013 Igor Yegorov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include "TestScene.h"

using namespace cocos2d;

CCScene* TestScene::scene() {
    auto scene = CCScene::create();
    auto layer = TestScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool TestScene::init() {
    if ( !CCLayer::init() ) {
        return false;
    }
    
    srand(time(nullptr));
    
    m_winSize = CCDirector::sharedDirector()->getWinSize();
    m_sizeFactor = m_winSize.height * 0.1f;
    
    auto label = CCLabelTTF::create("Just press the button - scale, anchor point and position will change randomly",
                                    "Arial",
                                    m_sizeFactor, m_winSize, kCCTextAlignmentCenter);
    label->setPosition(ccp(m_winSize.width * 0.5f, m_winSize.height * 0.35f));
    addChild(label);
    
    auto normal_sprite = createCircleColoredSprite(m_sizeFactor, ccc4FFromccc3B(ccGREEN));
    auto selected_sprite = createCircleColoredSprite(m_sizeFactor, ccc4FFromccc3B(ccGRAY));
    auto disabled_sprite = createCircleColoredSprite(m_sizeFactor, ccc4FFromccc3B(ccBLACK));
    
    normal_sprite->setScale(0.75f);
    selected_sprite->setScale(0.75f);
    disabled_sprite->setScale(0.75f);
    
    m_button = ContinuousPressButton::create(normal_sprite, selected_sprite, disabled_sprite);
    m_button->setPosition(m_winSize.width * 0.5f, m_winSize.height * 0.35f);
    
    m_button->setPressHandler([](){
        printf("Button pressed!\n");
    });
    
    m_button->setReleaseHandler([&](){
        printf("Button released!\n");
        
        auto button_anchor = ccp((rand() % 100) / 100.0f, (rand() % 100) / 100.0f);
        float button_x = rand() % int(m_winSize.width - 20) + 20;
        float button_y = rand() % int(m_winSize.height - 20) + 20;
        float button_scale_x = (rand() % 200) / 100.0f + 0.1f;
        float button_scale_y = (rand() % 200) / 100.0f + 0.1f;
        
        m_button->setPosition(button_x, button_y);
        m_button->setAnchorPoint(button_anchor);
        m_button->setScaleX(button_scale_x);
        m_button->setScaleY(button_scale_y);
        
        printf("\n================================================\n");
        printf("button position [%f:%f]\n", button_x, button_y);
        printf("button scale [%f:%f]\n", button_scale_x, button_scale_y);
        printf("button anchor [%f:%f]\n", button_anchor.x, button_anchor.y);
        printf("================================================\n\n");
        
    });
    
    addChild(m_button);
    
    return true;
}

CCSprite* TestScene::createCircleColoredSprite(float radius, cocos2d::ccColor4F const& color) const {
    auto draw_node = CCDrawNode::create();
    draw_node->drawDot(ccp(radius, radius), radius, color);
    
    auto const button_size = CCSize(radius * 2, radius * 2);
    
    auto sprite = CCSprite::create();
    sprite->setContentSize(button_size);
    sprite->addChild(draw_node);
    
    return sprite;
}