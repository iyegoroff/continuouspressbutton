/*
 * Copyright (C) 2013 Igor Yegorov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "ContinuousPressButton.h"
#include <cassert>

using namespace cocos2d;

ContinuousPressButton::ContinuousPressButton()
: m_normalSprite(nullptr)
, m_pressedSprite(nullptr)
, m_disabledSprite(nullptr)
, m_isEnabled(true)
, m_isPressed(false)
, m_releaseOnMoveOutside(false)
, m_pressHandler(nullptr)
, m_releaseHandler(nullptr) {}

ContinuousPressButton* ContinuousPressButton::create(cocos2d::CCSprite* normalSprite,
                                                     cocos2d::CCSprite* pressedSprite,
                                                     cocos2d::CCSprite* disabledSprite) {

    auto pRet = new ContinuousPressButton();
    if (pRet && pRet->init(normalSprite,
                           pressedSprite,
                           disabledSprite)) {

        pRet->autorelease();
    } else {
        CC_SAFE_DELETE(pRet);
    }

    return pRet;
}

bool ContinuousPressButton::init(cocos2d::CCSprite* normalSprite,
                                 cocos2d::CCSprite* pressedSprite,
                                 cocos2d::CCSprite* disabledSprite) {

    assert(!(m_normalSprite || m_pressedSprite || m_disabledSprite)
           && "Already initialized!");

    if (CCNode::init()) {
        m_normalSprite = normalSprite;
        m_pressedSprite = pressedSprite;
        m_disabledSprite = disabledSprite;

        m_normalSprite->setPosition(CCPointZero);
        m_pressedSprite->setPosition(CCPointZero);
        m_disabledSprite->setPosition(CCPointZero);

        addChild(pressedSprite);
        addChild(normalSprite);
        addChild(disabledSprite);

        auto size = m_normalSprite->getContentSize();
        size.width *= m_normalSprite->getScaleX();
        size.height *= m_normalSprite->getScaleY();

        setContentSize(size);

        disabledSprite->setVisible(false);

        return true;

    } else {
        return false;
    }
}

bool ContinuousPressButton::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent) {
    auto touchLocation = pTouch->getLocation();

    if (rect().containsPoint(touchLocation)) {
        press();
        return true;
    } else {
        return false;
    }
}

void ContinuousPressButton::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent) {
    auto touchLocation = pTouch->getLocation();

    if (!rect().containsPoint(touchLocation) && m_releaseOnMoveOutside) {
        release();
    }
}

void ContinuousPressButton::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent) {
    release();
}

void ContinuousPressButton::ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) {
    ccTouchEnded(pTouch, pEvent);
}

void ContinuousPressButton::onEnter() {
    CCNode::onEnter();

    auto pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, kCCMenuHandlerPriority, true);
}

void ContinuousPressButton::onExit() {
    CCNode::onExit();

    auto pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->removeDelegate(this);
}

void ContinuousPressButton::setEnabled(bool enabled) {
    m_isEnabled = enabled;
    m_disabledSprite->setVisible(!enabled);
    release();
}

void ContinuousPressButton::press() {
    if (m_isEnabled) {
        m_isPressed = true;
        m_normalSprite->setVisible(false);
        if (m_pressHandler) {
            m_pressHandler();
        }
    }
}

void ContinuousPressButton::release() {
    if (m_isPressed) {
        m_isPressed = false;
        m_normalSprite->setVisible(true);
        if (m_releaseHandler) {
            m_releaseHandler();
        }
    }
}

CCRect ContinuousPressButton::rect() {
    auto anchor_point = m_normalSprite->getAnchorPoint();

    return CCRectMake(m_obPosition.x - m_obContentSize.width * (anchor_point.x + m_obAnchorPoint.x) * m_fScaleX,
                      m_obPosition.y - m_obContentSize.height * (anchor_point.y + m_obAnchorPoint.y) * m_fScaleY,
                      m_obContentSize.width * m_fScaleX, m_obContentSize.height * m_fScaleY);
}
