/*
 * Copyright (C) 2013 Igor Yegorov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef CONTINUOUSPRESSBUTTON_H_
#define CONTINUOUSPRESSBUTTON_H_

#include <functional>
#include "cocos2d.h"

class ContinuousPressButton: public cocos2d::CCNode, public cocos2d::CCTargetedTouchDelegate {
public:
    typedef std::function<void()> Handler;

    ContinuousPressButton();
    virtual ~ContinuousPressButton() {}

    static ContinuousPressButton* create(
              cocos2d::CCSprite* normalSprite, // used to determine content size of the button
              cocos2d::CCSprite* pressedSprite,
              cocos2d::CCSprite* disabledSprite);

    bool init(cocos2d::CCSprite* normalSprite,
              cocos2d::CCSprite* pressedSprite,
              cocos2d::CCSprite* disabledSprite);

    /** Handler will be called when user presses the button */
    void setPressHandler(Handler pressHandler) { m_pressHandler = pressHandler; }

    /** Handler will be called when user releases the button */
    void setReleaseHandler(Handler releaseHandler) { m_releaseHandler = releaseHandler; }

    /** The button will be released when touch moves outside the button frame if enabled is true */
    void releaseOnMoveOutside(bool enabled) { m_releaseOnMoveOutside = enabled; }

    void setEnabled(bool enabled);
    bool isEnabled() const { return m_isEnabled; }

    bool isPressed() const { return m_isPressed; }

    virtual bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) override;
    virtual void ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) override;
    virtual void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) override;
    virtual void ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent) override;
    
    virtual void onEnter() override;
    virtual void onExit() override;

private:
    cocos2d::CCSprite* m_normalSprite;
    cocos2d::CCSprite* m_pressedSprite;
    cocos2d::CCSprite* m_disabledSprite;
    bool m_isEnabled;
    bool m_isPressed;
    bool m_releaseOnMoveOutside;

    Handler m_pressHandler;
    Handler m_releaseHandler;

    void press();
    void release();
    cocos2d::CCRect rect();
};

#endif /* CONTINUOUSPRESSBUTTON_H_ */
